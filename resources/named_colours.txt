# Name colour conbinations are taken from http://chir.ag/projects/ntc/ntc.js
Acadia	35312C
Acapulco	75AA94
Aero Blue	C0E8D5
Affair	745085
Afghan Tan	905E26
Air Force Blue	5D8AA8
Akaroa	BEB29A
Alabaster	F2F0E6
Albescent White	E1DACB
Alert Tan	954E2C
Alice Blue	F0F8FF
Alizarin	E32636
Allports	1F6A7D
Almond	EED9C4
Almond Frost	9A8678
Alpine	AD8A3B
Alto	CDC6C5
Aluminium	848789
Amaranth	E52B50
Amazon	387B54
Amber	FFBF00
Americano	8A7D72
Amethyst	9966CC
Amethyst Smoke	95879C
Amour	F5E6EA
Amulet	7D9D72
Anakiwa	8CCEEA
Antique Brass	6C461F
Antique White	FAEBD7
Anzac	C68E3F
Apache	D3A95C
Apple	66B348
Apple Blossom	A95249
Apple Green	DEEADC
Apricot	FBCEB1
Apricot White	F7F0DB
Aqua	00FFFF
Aqua Haze	D9DDD5
Aqua Spring	E8F3E8
Aqua Squeeze	DBE4DC
Aquamarine	7FFFD4
Arapawa	274A5D
Armadillo	484A46
Army green	4B5320
Arrowtown	827A67
Arsenic	3B444B
Ash	BEBAA7
Asparagus	7BA05B
Astra	EDD5A6
Astral	376F89
Astronaut	445172
Astronaut Blue	214559
Athens Grey	DCDDDD
Aths Special	D5CBB2
Atlantis	9CD03B
Atoll	2B797A
Atomic	3D4B52
Atomic Tangerine	FF9966
Au Chico	9E6759
Aubergine	372528
Auburn	712F2C
Australian Mint	EFF8AA
Avocado	95986B
Axolotl	63775A
Azalea	F9C0C4
Aztec	293432
Azure	F0FFFF
Baby Blue	6FFFFF
Bahama Blue	25597F
Bahia	A9C01C
Baker's Chocolate	5C3317
Bali Hai	849CA9
Baltic Sea	3C3D3E
Banana Mania	FBE7B2
Bandicoot	878466
Barberry	D2C61F
Barley Corn	B6935C
Barley White	F7E5B7
Barossa	452E39
Bastille	2C2C32
Battleship Grey	51574F
Bay Leaf	7BB18D
Bay Of Many	353E64
Bazaar	8F7777
Beauty Bush	EBB9B3
Beaver	926F5B
Beeswax	E9D7AB
Beige	F5F5DC
Bermuda	86D2C1
Bermuda Grey	6F8C9F
Beryl Green	BCBFA8
Bianca	F4EFE0
Big Stone	334046
Bilbao	3E8027
Biloba Flower	AE99D2
Birch	3F3726
Bird Flower	D0C117
Biscay	2F3C53
Bismark	486C7A
Bison Hide	B5AC94
Bisque	FFE4C4
Bistre	3D2B1F
Bitter	88896C
Bitter Lemon	D2DB32
Bittersweet	FE6F5E
Bizarre	E7D2C8
Black	000000
Black Bean	232E26
Black Forest	2C3227
Black Haze	E0DED7
Black Magic	332C22
Black Marlin	383740
Black Pearl	1E272C
Black Rock	2C2D3C
Black Rose	532934
Black Russian	24252B
Black Squeeze	E5E6DF
Black White	E5E4DB
Blackberry	43182F
Blackcurrant	2E183B
Blanc	D9D0C1
Blanched Almond	FFEBCD
Bleach White	EBE1CE
Blizzard Blue	A3E3ED
Blossom	DFB1B6
Blue	0000FF
Blue Bayoux	62777E
Blue Bell	9999CC
Blue Chalk	E3D6E9
Blue Charcoal	262B2F
Blue Chill	408F90
Blue Diamond	4B2D72
Blue Dianne	35514F
Blue Gem	4B3C8E
Blue Haze	BDBACE
Blue Lagoon	00626F
Blue Marguerite	6A5BB1
Blue Romance	D8F0D2
Blue Smoke	78857A
Blue Stone	166461
Blue Violet	8A2BE2
Blue Whale	1E3442
Blue Zodiac	3C4354
Blumine	305C71
Blush	B55067
Bokara Grey	2A2725
Bole	79443B
Bombay	AEAEAD
Bon Jour	DFD7D2
Bondi Blue	0095B6
Bone	DBC2AB
Bordeaux	4C1C24
Bossanova	4C3D4E
Boston Blue	438EAC
Botticelli	92ACB4
Bottle Green	254636
Boulder	7C817C
Bouquet	A78199
Bourbon	AF6C3E
Bracken	5B3D27
Brandy	DCB68A
Brandy Punch	C07C40
Brandy Rose	B6857A
Brass	B5A642
Breaker Bay	517B78
Brick Red	C62D42
Bridal Heath	F8EBDD
Bridesmaid	FAE6DF
Bright Green	66FF00
Bright Grey	57595D
Bright Red	922A31
Bright Sun	ECBD2C
Bright Turquoise	08E8DE
Brilliant Rose	FF55A3
Brink Pink	FB607F
British Racing Green	004225
Bronco	A79781
Bronze	CD7F32
Bronze Olive	584C25
Bronzetone	434C28
Broom	EECC24
Brown	A52A2A
Brown Bramble	53331E
Brown Derby	594537
Brown Pod	3C241B
Bubbles	E6F2EA
Buccaneer	6E5150
Bud	A5A88F
Buddha Gold	BC9B1B
Buff	F0DC82
Bulgarian Rose	482427
Bull Shot	75442B
Bunker	292C2F
Bunting	2B3449
Burgundy	800020
Burly Wood	DEB887
Burnham	234537
Burning Sand	D08363
Burnt Crimson	582124
Burnt Orange	FF7034
Burnt Sienna	E97451
Burnt Umber	8A3324
Buttercup	DA9429
Buttered Rum	9D702E
Butterfly Bush	68578C
Buttermilk	F6E0A4
Buttery White	F1EBDA
Cab Sav	4A2E32
Cabaret	CD526C
Cabbage Pont	4C5544
Cactus	5B6F55
Cadet Blue	5F9EA0
Cadillac	984961
Cafe Royale	6A4928
Calico	D5B185
California	E98C3A
Calypso	3D7188
Camarone	206937
Camelot	803A4B
Cameo	CCA483
Camouflage	4F4D32
Camouflage Green	78866B
Can Can	D08A9B
Canary	FFFF99
Cannon Pink	8E5164
Cape Cod	4E5552
Cape Honey	FEE0A5
Cape Palliser	75482F
Caper	AFC182
Caput Mortuum	592720
Caramel	FFD59A
Cararra	EBE5D5
Cardin Green	1B3427
Cardinal	C41E3A
Careys Pink	C99AA0
Caribbean Green	00CC99
Carissma	E68095
Carla	F5F9CB
Carmine	960018
Carnaby Tan	5B3A24
Carnation Pink	FFA6C9
Carousel Pink	F8DBE0
Carrot Orange	ED9121
Casablanca	F0B253
Casal	3F545A
Cascade	8CA8A0
Cashmere	D1B399
Casper	AAB5B8
Castro	44232F
Catalina Blue	273C5A
Catskill White	E0E4DC
Cavern Pink	E0B8B1
Ce Soir	9271A7
Cedar	463430
Celadon	ACE1AF
Celery	B4C04C
Celeste	D2D2C0
Cello	3A4E5F
Celtic	2B3F36
Cement	857158
Cerise	DE3163
Cerulean	007BA7
Cerulean Blue	2A52BE
Chablis	FDE9E0
Chalet Green	5A6E41
Chalky	DFC281
Chambray	475877
Chamois	E8CD9A
Champagne	EED9B6
Chantilly	EDB8C7
Charade	394043
Charcoal	464646
Chardon	F8EADF
Chardonnay	FFC878
Charlotte	A4DCE6
Charm	D0748B
Chartreuse	7FFF00
Chartreuse Yellow	DFFF00
Chateau Green	419F59
Chatelle	B3ABB6
Chathams Blue	2C5971
Chelsea Cucumber	88A95B
Chelsea Gem	95532F
Chenin	DEC371
Cherokee	F5CD82
Cherry Pie	372D52
Cherub	F5D7DC
Chestnut	B94E48
Chetwode Blue	666FB4
Chicago	5B5D56
Chiffon	F0F5BB
Chilean Fire	D05E34
Chilean Heath	F9F7DE
China Ivory	FBF3D3
Chino	B8AD8A
Chinook	9DD3A8
Chocolate	D2691E
Christalle	382161
Christi	71A91D
Christine	BF652E
Chrome White	CAC7B7
Cigar	7D4E38
Cinder	242A2E
Cinderella	FBD7CC
Cinnabar	E34234
Cioccolato	5D3B2E
Citron	8E9A21
Citrus	9FB70A
Clam Shell	D2B3A9
Claret	6E2233
Classic Rose	F4C8DB
Clay Creek	897E59
Clear Day	DFEFEA
Clinker	463623
Cloud	C2BCB1
Cloud Burst	353E4F
Cloudy	B0A99F
Clover	47562F
Cobalt	0047AB
Cocoa Bean	4F3835
Cocoa Brown	35281E
Coconut Cream	E1DABB
Cod Grey	2D3032
Coffee	726751
Coffee Bean	362D26
Cognac	9A463D
Cola	3C2F23
Cold Purple	9D8ABF
Cold Turkey	CAB5B2
Columbia Blue	9BDDFF
Comet	636373
Como	4C785C
Conch	A0B1AE
Concord	827F79
Concrete	D2D1CD
Confetti	DDCB46
Congo Brown	654D49
Conifer	B1DD52
Contessa	C16F68
Copper	DA8A67
Copper Canyon	77422C
Copper Rose	996666
Copper Rust	95524C
Coral	FF7F50
Coral Candy	F5D0C9
Coral Red	FF4040
Coral Tree	AB6E67
Corduroy	404D49
Coriander	BBB58D
Cork	5A4C42
Corn	FBEC5D
Corn Field	F8F3C4
Corn Flower Blue	42426F
Corn Harvest	8D702A
Corn Silk	FFF8DC
Cornflower	93CCEA
Cornflower Blue	6495ED
Corvette	E9BA81
Cosmic	794D60
Cosmic Latte	E1F8E7
Cosmos	FCD5CF
Costa Del Sol	625D2A
Cotton Candy	FFB7D5
Cotton Seed	BFBAAF
County Green	1B4B35
Cowboy	443736
Crab Apple	87382F
Crail	A65648
Cranberry	DB5079
Crater Brown	4D3E3C
Cream	FFFDD0
Cream Brulee	FFE39B
Cream Can	EEC051
Creole	393227
Crete	77712B
Crimson	DC143C
Crocodile	706950
Crown Of Thorns	763C33
Cruise	B4E2D5
Crusoe	165B31
Crusta	F38653
Cumin	784430
Cumulus	F5F4C1
Cupid	F5B2C5
Curious Blue	3D85B8
Cutty Sark	5C8173
Cyprus	0F4645
Dairy Cream	EDD2A4
Daisy Bush	5B3E90
Dallas	664A2D
Dandelion	FED85D
Danube	5B89C0
Dark Blue	00008B
Dark Brown	654321
Dark Cerulean	08457E
Dark Chestnut	986960
Dark Coral	CD5B45
Dark Cyan	008B8B
Dark Goldenrod	B8860B
Dark Gray	A9A9A9
Dark Green	013220
Dark Green Copper	4A766E
Dark Khaki	BDB76B
Dark Magenta	8B008B
Dark Olive Green	556B2F
Dark Orange	FF8C00
Dark Orchid	9932CC
Dark Pastel Green	03C03C
Dark Pink	E75480
Dark Purple	871F78
Dark Red	8B0000
Dark Rum	45362B
Dark Salmon	E9967A
Dark Sea Green	8FBC8F
Dark Slate	465352
Dark Slate Blue	483D8B
Dark Slate Grey	2F4F4F
Dark Spring Green	177245
Dark Tan	97694F
Dark Tangerine	FFA812
Dark Turquoise	00CED1
Dark Violet	9400D3
Dark Wood	855E42
Davy's Grey	788878
Dawn	9F9D91
Dawn Pink	E6D6CD
De York	85CA87
Deco	CCCF82
Deep Blush	E36F8A
Deep Bronze	51412D
Deep Cerise	DA3287
Deep Fir	193925
Deep Koamaru	343467
Deep Lilac	9955BB
Deep Magenta	CC00CC
Deep Pink	FF1493
Deep Sea	167E65
Deep Sky Blue	00BFFF
Deep Teal	19443C
Del Rio	B5998E
Dell	486531
Delta	999B95
Deluge	8272A4
Denim	1560BD
Derby	F9E4C6
Desert	A15F3B
Desert Sand	EDC9AF
Desert Storm	EDE7E0
Dew	E7F2E9
Diesel	322C2B
Dim Gray	696969
Dingley	607C47
Disco	892D4F
Dixie	CD8431
Dodger Blue	1E90FF
Dolly	F5F171
Dolphin	6A6873
Domino	6C5B4C
Don Juan	5A4F51
Donkey Brown	816E5C
Dorado	6E5F56
Double Colonial White	E4CF99
Double Pearl Lusta	E9DCBE
Double Spanish White	D2C3A3
Dove Grey	777672
Downy	6FD2BE
Drover	FBEB9B
Dune	514F4A
Dust Storm	E5CAC0
Dusty Grey	AC9B9B
Dutch White	F0DFBB
Eagle	B0AC94
Earls Green	B8A722
Early Dawn	FBF2DB
East Bay	47526E
East Side	AA8CBC
Eastern Blue	00879F
Ebb	E6D8D4
Ebony	313337
Ebony Clay	323438
Echo Blue	A4AFCD
Eclipse	3F3939
Ecru	C2B280
Ecru White	D6D1C0
Ecstasy	C96138
Eden	266255
Edgewater	C1D8C5
Edward	97A49A
Egg Sour	F9E4C5
Eggplant	990066
Egyptian Blue	1034A6
El Paso	39392C
El Salva	8F4E45
Electric Blue	7DF9FF
Electric Indigo	6600FF
Electric Lime	CCFF00
Electric Purple	BF00FF
Elephant	243640
Elf Green	1B8A6B
Elm	297B76
Emerald	50C878
Eminence	6E3974
Emperor	50494A
Empress	7C7173
Endeavour	29598B
Energy Yellow	F5D752
English Holly	274234
Envy	8BA58F
Equator	DAB160
Espresso	4E312D
Eternity	2D2F28
Eucalyptus	329760
Eunry	CDA59C
Evening Sea	26604F
Everglade	264334
Fair Pink	F3E5DC
Falcon	6E5A5B
Fallow	C19A6B
Falu Red	801818
Fantasy	F2E6DD
Fedora	625665
Feijoa	A5D785
Feldgrau	4D5D53
Feldspar	D19275
Fern	63B76C
Fern Green	4F7942
Ferra	876A68
Festival	EACC4A
Feta	DBE0D0
Fiery Orange	B1592F
Fiji Green	636F22
Finch	75785A
Finlandia	61755B
Finn	694554
Fiord	4B5A62
Fire	8F3F2A
Fire Brick	B22222
Fire Bush	E09842
Fire Engine Red	CE1620
Firefly	314643
Flame Pea	BE5C48
Flame Red	86282E
Flamenco	EA8645
Flamingo	E1634F
Flax	EEDC82
Flint	716E61
Flirt	7A2E4D
Floral White	FFFAF0
Foam	D0EAE8
Fog	D5C7E8
Foggy Grey	A7A69D
Forest Green	228B22
Forget Me Not	FDEFDB
Fountain Blue	65ADB2
Frangipani	FFD7A0
Free Speech Aquamarine	029D74
Free Speech Blue	4156C5
Free Speech Green	09F911
Free Speech Magenta	E35BD8
Free Speech Red	C00000
French Grey	BFBDC1
French Lilac	DEB7D9
French Pass	A4D2E0
French Rose	F64A8A
Friar Grey	86837A
Fringy Flower	B4E1BB
Froly	E56D75
Frost	E1E4C5
Frosted Mint	E2F2E4
Frostee	DBE5D2
Fruit Salad	4BA351
Fuchsia	C154C1
Fuchsia Pink	FF77FF
Fuego	C2D62E
Fuel Yellow	D19033
Fun Blue	335083
Fun Green	15633D
Fuscous Grey	3C3B3C
Fuzzy Wuzzy Brown	C45655
Gable Green	2C4641
Gainsboro	DCDCDC
Gallery	DCD7D1
Galliano	D8A723
Gamboge	E49B0F
Geebung	C5832E
Genoa	31796D
Geraldine	E77B75
Geyser	CBD0CF
Ghost	C0BFC7
Ghost White	F8F8FF
Gigas	564786
Gimblet	B9AD61
Gin	D9DFCD
Gin Fizz	F8EACA
Givry	EBD4AE
Glacier	78B1BF
Glade Green	5F8151
Go Ben	786E4C
Goblin	34533D
Gold	FFD700
Gold Drop	D56C30
Gold Tips	E2B227
Golden Bell	CA8136
Golden Brown	996515
Golden Dream	F1CC2B
Golden Fizz	EBDE31
Golden Glow	F9D77E
Golden Poppy	FCC200
Golden Sand	EACE6A
Golden Tainoi	FFC152
Golden Yellow	FFDF00
Goldenrod	DBDB70
Gondola	373332
Gordons Green	29332B
Gorse	FDE336
Gossamer	399F86
Gossip	9FD385
Gothic	698890
Governor Bay	51559B
Grain Brown	CAB8A2
Grandis	FFCD73
Granite Green	8B8265
Granny Apple	C5E7CD
Granny Smith	7B948C
Granny Smith Apple	9DE093
Grape	413D4B
Graphite	383428
Gravel	4A4B46
Green	008000
Green House	3E6334
Green Kelp	393D2A
Green Leaf	526B2D
Green Mist	BFC298
Green Pea	266242
Green Smoke	9CA664
Green Spring	A9AF99
Green Vogue	23414E
Green Waterloo	2C2D24
Green White	DEDDCB
Green Yellow	ADFF2F
Grenadier	C14D36
Grey	808080
Grey Chateau	9FA3A7
Grey Nickel	BDBAAE
Grey Nurse	D1D3CC
Grey Olive	A19A7F
Grey Suit	9391A0
Grey-Asparagus	465945
Guardsman Red	952E31
Gulf Blue	343F5C
Gulf Stream	74B2A8
Gull Grey	A4ADB0
Gum Leaf	ACC9B2
Gumbo	718F8A
Gun Powder	484753
Gunmetal	2C3539
Gunsmoke	7A7C76
Gurkha	989171
Hacienda	9E8022
Hairy Heath	633528
Haiti	2C2A35
Half And Half	EDE7C8
Half Baked	558F93
Half Colonial White	F2E5BF
Half Dutch White	FBF0D6
Half Pearl Lusta	F1EAD7
Half Spanish White	E6DBC7
Hampton	E8D4A2
Han Purple	5218FA
Harlequin	3FFF00
Harley Davidson Orange	C93413
Harp	CBCEC0
Harvest Gold	EAB76A
Havana	3B2B2C
Havelock Blue	5784C1
Hawaiian Tan	99522B
Hawkes Blue	D2DAED
Heath	4F2A2C
Heather	AEBBC1
Heathered Grey	948C7E
Heavy Metal	46473E
Heliotrope	DF73FF
Hemlock	69684B
Hemp	987D73
Highball	928C3C
Highland	7A9461
Hillary	A7A07E
Himalaya	736330
Hint Of Green	DFF1D6
Hint Of Red	F5EFEB
Hint Of Yellow	F6F5D7
Hippie Blue	49889A
Hippie Green	608A5A
Hippie Pink	AB495C
Hit Grey	A1A9A8
Hit Pink	FDA470
Hokey Pokey	BB8E34
Hoki	647D86
Holly	25342B
Hollywood Cerise	F400A1
Honey Flower	5C3C6D
Honeydew	F0FFF0
Honeysuckle	E8ED69
Hopbush	CD6D93
Horizon	648894
Horses Neck	6D562C
Hot Curry	815B28
Hot Magenta	FF00CC
Hot Pink	FF69B4
Hot Purple	4E2E53
Hot Toddy	A7752C
Humming Bird	CEEFE4
Hunter Green	355E3B
Hurricane	8B7E77
Husk	B2994B
Ice Cold	AFE3D6
Iceberg	CAE1D9
Illusion	EF95AE
Inch Worm	B0E313
Indian Red	CD5C5C
Indian Tan	4F301F
Indigo	4B0082
Indochine	9C5B34
International Klein Blue	002FA7
International Orange	FF4F00
Iris Blue	03B4C8
Irish Coffee	62422B
Iron	CBCDCD
Ironside Grey	706E66
Ironstone	865040
Islamic Green	009900
Island Spice	F8EDDB
Ivory	FFFFF0
Jacarta	3D325D
Jacko Bean	413628
Jacksons Purple	3D3F7D
Jade	00A86B
Jaffa	E27945
Jagged Ice	CAE7E2
Jagger	3F2E4C
Jaguar	29292F
Jambalaya	674834
Japanese Laurel	2F7532
Japonica	CE7259
Java	259797
Jazz	5F2C2F
Jazzberry Jam	A50B5E
Jelly Bean	44798E
Jet Stream	BBD0C9
Jewel	136843
Jon	463D3E
Jonquil	EEF293
Jordy Blue	7AAAE0
Judge Grey	5D5346
Jumbo	878785
Jungle Green	29AB87
Jungle Mist	B0C4C4
Juniper	74918E
Just Right	DCBFAC
Kabul	6C5E53
Kaitoke Green	245336
Kangaroo	C5C3B0
Karaka	2D2D24
Karry	FEDCC1
Kashmir Blue	576D8E
Kelly Green	4CBB17
Kelp	4D503C
Kenyan Copper	6C322E
Keppel	5FB69C
Khaki	F0E68C
Kidnapper	BFC0AB
Kilamanjaro	3A3532
Killarney	49764F
Kimberly	695D87
Kingfisher Daisy	583580
Kobi	E093AB
Kokoda	7B785A
Korma	804E2C
Koromiko	FEB552
Kournikova	F9D054
La Palma	428929
La Rioja	BAC00E
Las Palmas	C6DA36
Laser	C6A95E
Laser Lemon	FFFF66
Laurel	6E8D71
Lavender	E6E6FA
Lavender Blue	CCCCFF
Lavender Blush	FFF0F5
Lavender Grey	BDBBD7
Lavender Pink	FBAED2
Lavender Rose	FBA0E3
Lawn Green	7CFC00
Leather	906A54
Lemon	FDE910
Lemon Chiffon	FFFACD
Lemon Ginger	968428
Lemon Grass	999A86
Licorice	2E3749
Light Blue	ADD8E6
Light Coral	F08080
Light Cyan	E0FFFF
Light Goldenrod	EEDD82
Light Goldenrod Yellow	FAFAD2
Light Green	90EE90
Light Grey	D3D3D3
Light Pink	FFB6C1
Light Salmon	FFA07A
Light Sea Green	20B2AA
Light Sky Blue	87CEFA
Light Slate Blue	8470FF
Light Slate Grey	778899
Light Steel Blue	B0C4DE
Light Wood	856363
Light Yellow	FFFFE0
Lightning Yellow	F7A233
Lilac	C8A2C8
Lilac Bush	9470C4
Lily	C19FB3
Lily White	E9EEEB
Lima	7AAC21
Lime	00FF00
Lime Green	32CD32
Limeade	5F9727
Limerick	89AC27
Linen	FAF0E6
Link Water	C7CDD8
Lipstick	962C54
Liver	534B4F
Livid Brown	312A29
Loafer	DBD9C2
Loblolly	B3BBB7
Lochinvar	489084
Lochmara	316EA0
Locust	A2A580
Log Cabin	393E2E
Logan	9D9CB4
Lola	B9ACBB
London Hue	AE94AB
Lonestar	522426
Lotus	8B504B
Loulou	4C3347
Lucky	AB9A1C
Lucky Point	292D4F
Lunar Green	4E5541
Lusty	782E2C
Luxor Gold	AB8D3F
Lynch	697D89
Mabel	CBE8E8
Macaroni And Cheese	FFB97B
Madang	B7E3A8
Madison	2D3C54
Madras	473E23
Magenta	FF00FF
Magic Mint	AAF0D1
Magnolia	F8F4FF
Mahogany	CA3435
Mai Tai	A56531
Maire	2A2922
Maize	E3B982
Makara	695F50
Mako	505555
Malachite	0BDA51
Malachite Green	97976F
Malibu	66B7E1
Mallard	3A4531
Malta	A59784
Mamba	766D7C
Manatee	8D90A1
Mandalay	B57B2E
Mandarian Orange	8E2323
Mandy	CD525B
Mandys Pink	F5B799
Mango Tango	E77200
Manhattan	E2AF80
Mantis	7FC15C
Mantle	96A793
Manz	E4DB55
Mardi Gras	352235
Marigold	B88A3D
Mariner	42639F
Maroon	800000
Marshland	2B2E26
Martini	B7A8A3
Martinique	3C3748
Marzipan	EBC881
Masala	57534B
Matisse	365C7D
Matrix	8E4D45
Matterhorn	524B4B
Mauve	E0B0FF
Mauve Taupe	915F6D
Mauvelous	F091A9
Maverick	C8B1C0
Maya Blue	73C2FB
McKenzie	8C6338
Medium Aquamarine	66CDAA
Medium Blue	0000CD
Medium Carmine	AF4035
Medium Goldenrod	EAEAAE
Medium Orchid	BA55D3
Medium Purple	9370DB
Medium Sea Green	3CB371
Medium Slate Blue	7B68EE
Medium Spring Green	00FA9A
Medium Turquoise	48D1CC
Medium Violet Red	C71585
Medium Wood	A68064
Melanie	E0B7C2
Melanzane	342931
Melon	FEBAAD
Melrose	C3B9DD
Mercury	D5D2D1
Merino	E1DBD0
Merlin	4F4E48
Merlot	73343A
Metallic Bronze	554A3C
Metallic Copper	6E3D34
Metallic Gold	D4AF37
Meteor	BB7431
Meteorite	4A3B6A
Mexican Red	9B3D3D
Mid Grey	666A6D
Midnight	21303E
Midnight Blue	191970
Midnight Express	21263A
Midnight Moss	242E28
Mikado	3F3623
Milan	F6F493
Milano Red	9E3332
Milk Punch	F3E5C0
Milk White	DCD9CD
Millbrook	595648
Mimosa	F5F5CC
Mindaro	DAEA6F
Mine Shaft	373E41
Mineral Green	506355
Ming	407577
Minsk	3E3267
Mint Cream	F5FFFA
Mint Green	98FF98
Mint Julep	E0D8A7
Mint Tulip	C6EADD
Mirage	373F43
Mischka	A5A9B2
Mist Grey	BAB9A9
Misty Rose	FFE4E1
Mobster	605A67
Moccaccino	582F2B
Moccasin	FFE4B5
Mocha	6F372D
Mojo	97463C
Mona Lisa	FF9889
Monarch	6B252C
Mondo	554D42
Mongoose	A58B6F
Monsoon	7A7679
Montana	393B3C
Monte Carlo	7AC5B4
Moody Blue	8378C7
Moon Glow	F5F3CE
Moon Mist	CECDB8
Moon Raker	C0B2D7
Moon Yellow	F0C420
Morning Glory	9ED1D3
Morocco Brown	442D21
Mortar	565051
Mosque	005F5B
Moss Green	ADDFAD
Mountain Meadow	1AB385
Mountain Mist	A09F9C
Mountbatten Pink	997A8D
Muddy Waters	A9844F
Muesli	9E7E53
Mulberry	C54B8C
Mule Fawn	884F40
Mulled Wine	524D5B
Mustard	FFDB58
My Pink	D68B80
My Sin	FDAE45
Myrtle	21421E
Mystic	D8DDDA
Nandor	4E5D4E
Napa	A39A87
Narvik	E9E6DC
Navajo White	FFDEAD
Navy	000080
Navy Blue	0066CC
Nebula	B8C6BE
Negroni	EEC7A2
Neon Blue	4D4DFF
Neon Carrot	FF9933
Neon Pink	FF6EC7
Nepal	93AAB9
Neptune	77A8AB
Nero	252525
Neutral Green	AAA583
Nevada	666F6F
New Amber	6D3B24
New Midnight Blue	00009C
New Orleans	E4C385
New Tan	EBC79E
New York Pink	DD8374
Niagara	29A98B
Night Rider	332E2E
Night Shadz	A23D54
Nile Blue	253F4E
Nobel	A99D9D
Nomad	A19986
Nordic	1D393C
Norway	A4B88F
Nugget	BC9229
Nutmeg	7E4A3B
Oasis	FCEDC5
Observatory	008F70
Ocean Green	4CA973
Ochre	CC7722
Off Green	DFF0E2
Off Yellow	FAF3DC
Oil	313330
Old Brick	8A3335
Old Copper	73503B
Old Gold	CFB53B
Old Lace	FDF5E6
Old Lavender	796878
Old Rose	C02E4C
Olive	808000
Olive Drab	6B8E23
Olive Green	B5B35C
Olive Haze	888064
Olivetone	747028
Olivine	9AB973
Onahau	C2E6EC
Onion	48412B
Opal	A8C3BC
Opium	987E7E
Oracle	395555
Orange	FFA500
Orange Peel	FFA000
Orange Red	FF4500
Orange Roughy	A85335
Orange White	EAE3CD
Orchid	DA70D6
Orchid White	F1EBD9
Orient	255B77
Oriental Pink	C28E88
Orinoco	D2D3B3
Oslo Grey	818988
Ottoman	D3DBCB
Outer Space	2D383A
Outrageous Orange	FF6037
Oxford Blue	28353A
Oxley	6D9A78
Oyster Bay	D1EAEA
Oyster Pink	D4B5B0
Paarl	864B36
Pablo	7A715C
Pacific Blue	009DC4
Paco	4F4037
Padua	7EB394
Palatinate Purple	682860
Pale Brown	987654
Pale Chestnut	DDADAF
Pale Cornflower Blue	ABCDEF
Pale Goldenrod	EEE8AA
Pale Green	98FB98
Pale Leaf	BDCAA8
Pale Magenta	F984E5
Pale Oyster	9C8D72
Pale Pink	FADADD
Pale Prim	F9F59F
Pale Rose	EFD6DA
Pale Sky	636D70
Pale Slate	C3BEBB
Pale Taupe	BC987E
Pale Turquoise	AFEEEE
Pale Violet Red	DB7093
Palm Green	20392C
Palm Leaf	36482F
Pampas	EAE4DC
Panache	EBF7E4
Pancho	DFB992
Panda	544F3A
Papaya Whip	FFEFD5
Paprika	7C2D37
Paradiso	488084
Parchment	D0C8B0
Paris Daisy	FBEB50
Paris M	312760
Paris White	BFCDC0
Parsley	305D35
Pastel Green	77DD77
Patina	639283
Pattens Blue	D3E5EF
Paua	2A2551
Pavlova	BAAB87
Payne's Grey	404048
Peach	FFCBA4
Peach Puff	FFDAB9
Peach-Orange	FFCC99
Peach-Yellow	FADFAD
Peanut	7A4434
Pear	D1E231
Pearl Bush	DED1C6
Pearl Lusta	EAE0C8
Peat	766D52
Pelorous	2599B2
Peppermint	D7E7D0
Perano	ACB9E8
Perfume	C2A9DB
Periglacial Blue	ACB6B2
Periwinkle	C3CDE6
Persian Blue	1C39BB
Persian Green	00A693
Persian Indigo	32127A
Persian Pink	F77FBE
Persian Plum	683332
Persian Red	CC3333
Persian Rose	FE28A2
Persimmon	EC5800
Peru	CD853F
Peru Tan	733D1F
Pesto	7A7229
Petite Orchid	DA9790
Pewter	91A092
Pharlap	826663
Picasso	F8EA97
Picton Blue	5BA0D0
Pig Pink	FDD7E4
Pigment Green	00A550
Pine Cone	756556
Pine Glade	BDC07E
Pine Green	01796F
Pine Tree	2A2F23
Pink	FFC0CB
Pink Flamingo	FF66FF
Pink Flare	D8B4B6
Pink Lace	F6CCD7
Pink Lady	F3D7B6
Pink Swan	BFB3B2
Piper	9D5432
Pipi	F5E6C4
Pippin	FCDBD2
Pirate Gold	BA782A
Pixie Green	BBCDA5
Pizazz	E57F3D
Pizza	BF8D3C
Plantation	3E594C
Plum	DDA0DD
Pohutukawa	651C26
Polar	E5F2E7
Polo Blue	8AA7CC
Pompadour	6A1F44
Porcelain	DDDCDB
Porsche	DF9D5B
Port Gore	3B436C
Portafino	F4F09B
Portage	8B98D8
Portica	F0D555
Pot Pourri	EFDCD4
Potters Clay	845C40
Powder Blue	B0E0E6
Prairie Sand	883C32
Prelude	CAB4D4
Prim	E2CDD5
Primrose	E4DE8E
Promenade	F8F6DF
Provincial Pink	F6E3DA
Prussian Blue	003366
Psychedelic Purple	DD00FF
Puce	CC8899
Pueblo	6E3326
Puerto Rico	59BAA3
Pumice	BAC0B4
Pumpkin	FF7518
Punga	534931
Purple	800080
Purple Heart	652DC1
Purple Mountain's Majesty	9678B6
Purple Taupe	50404D
Putty	CDAE70
Quarter Pearl Lusta	F2EDDD
Quarter Spanish White	EBE2D2
Quartz	D9D9F3
Quicksand	C3988B
Quill Grey	CBC9C0
Quincy	6A5445
Racing Green	232F2C
Radical Red	FF355E
Raffia	DCC6A0
Rain Forest	667028
Rainee	B3C1B1
Rajah	FCAE60
Rangoon Green	2B2E25
Raven	6F747B
Raw Sienna	D27D46
Raw Umber	734A12
Razzle Dazzle Rose	FF33CC
Razzmatazz	E30B5C
Rebel	453430
Red	FF0000
Red Berry	701F28
Red Damask	CB6F4A
Red Devil	662A2C
Red Orange	FF3F34
Red Oxide	5D1F1E
Red Robin	7D4138
Red Stage	AD522E
Medium Red Violet	BB3385
Redwood	5B342E
Reef	D1EF9F
Reef Gold	A98D36
Regal Blue	203F58
Regent Grey	798488
Regent St Blue	A0CDD9
Remy	F6DEDA
Reno Sand	B26E33
Resolution Blue	323F75
Revolver	37363F
Rhino	3D4653
Rice Cake	EFECDE
Rice Flower	EFF5D1
Rich Blue	5959AB
Rich Gold	A15226
Rio Grande	B7C61A
Riptide	89D9C8
River Bed	556061
Rob Roy	DDAD56
Robin's Egg Blue	00CCCC
Rock	5A4D41
Rock Blue	93A2BA
Rock Spray	9D442D
Rodeo Dust	C7A384
Rolling Stone	6D7876
Roman	D8625B
Roman Coffee	7D6757
Romance	F4F0E6
Romantic	FFC69E
Ronchi	EAB852
Roof Terracotta	A14743
Rope	8E593C
Rose	D3A194
Rose Bud	FEAB9A
Rose Bud Cherry	8A2D52
Rose Of Sharon	AC512D
Rose Taupe	905D5D
Rose White	FBEEE8
Rosy Brown	BC8F8F
Roti	B69642
Rouge	A94064
Royal Blue	4169E1
Royal Heath	B54B73
Royal Purple	6B3FA0
Ruby	E0115F
Rum	716675
Rum Swizzle	F1EDD4
Russet	80461B
Russett	7D655C
Rust	B7410E
Rustic Red	3A181A
Rusty Nail	8D5F2C
Saddle	5D4E46
Saddle Brown	8B4513
Safety Orange	FF6600
Saffron	F4C430
Sage	989F7A
Sahara	B79826
Sail	A5CEEC
Salem	177B4D
Salmon	FA8072
Salomie	FFD67B
Salt Box	696268
Saltpan	EEF3E5
Sambuca	3B2E25
San Felix	2C6E31
San Juan	445761
San Marino	4E6C9D
Sand Dune	867665
Sandal	A3876A
Sandrift	AF937D
Sandstone	786D5F
Sandwisp	DECB81
Sandy Beach	FEDBB7
Sandy Brown	F4A460
Sangria	92000A
Sanguine Brown	6C3736
Santas Grey	9998A7
Sante Fe	A96A50
Sapling	E1D5A6
Sapphire	082567
Saratoga	555B2C
Sauvignon	F4EAE4
Sazerac	F5DEC4
Scampi	6F63A0
Scandal	ADD9D1
Scarlet	FF2400
Scarlet Gum	4A2D57
Scarlett	7E2530
Scarpa Flow	6B6A6C
Schist	87876F
School Bus Yellow	FFD800
Schooner	8D8478
Scooter	308EA0
Scorpion	6A6466
Scotch Mist	EEE7C8
Screamin' Green	66FF66
Scrub	3D4031
Sea Buckthorn	EF9548
Sea Fog	DFDDD6
Sea Green	2E8B57
Sea Mist	C2D5C4
Sea Nymph	8AAEA4
Sea Pink	DB817E
Seagull	77B7D0
Seal Brown	321414
Seance	69326E
Seashell	FFF5EE
Seaweed	37412A
Selago	E6DFE7
Selective Yellow	FFBA00
Semi-Sweet Chocolate	6B4226
Sepia	9E5B40
Serenade	FCE9D7
Shadow	837050
Shadow Green	9AC0B6
Shady Lady	9F9B9D
Shakespeare	609AB8
Shalimar	F8F6A8
Shamrock	33CC99
Shamrock Green	009E60
Shark	34363A
Sherpa Blue	00494E
Sherwood Green	1B4636
Shilo	E6B2A6
Shingle Fawn	745937
Ship Cove	7988AB
Ship Grey	4E4E4C
Shiraz	842833
Shocking	E899BE
Shocking Pink	FC0FC0
Shuttle Grey	61666B
Siam	686B50
Sidecar	E9D9A9
Sienna	A0522D
Silk	BBADA1
Silver	C0C0C0
Silver Chalice	ACAEA9
Silver Sand	BEBDB6
Silver Tree	67BE90
Sinbad	A6D5D0
Siren	69293B
Sirocco	68766E
Sisal	C5BAA0
Skeptic	9DB4AA
Sky Blue	87CEEB
Slate Blue	6A5ACD
Slate Grey	708090
Slugger	42342B
Smalt	003399
Smalt Blue	496267
Smoke Tree	BB5F34
Smoky	605D6B
Snow	FFFAFA
Snow Drift	E3E3DC
Snow Flurry	EAF7C9
Snowy Mint	D6F0CD
Snuff	E4D7E5
Soapstone	ECE5DA
Soft Amber	CFBEA5
Soft Peach	EEDFDE
Solid Pink	85494C
Solitaire	EADAC2
Solitude	E9ECF1
Sorbus	DD6B38
Sorrell Brown	9D7F61
Sour Dough	C9B59A
Soya Bean	6F634B
Space Shuttle	4B433B
Spanish Green	7B8976
Spanish White	DED1B7
Spectra	375D4F
Spice	6C4F3F
Spicy Mix	8B5F4D
Spicy Pink	FF1CAE
Spindle	B3C4D8
Splash	F1D79E
Spray	7ECDDD
Spring Bud	A7FC00
Spring Green	00FF7F
Spring Rain	A3BD9C
Spring Sun	F1F1C6
Spring Wood	E9E1D9
Sprout	B8CA9D
Spun Pearl	A2A1AC
Squirrel	8F7D6B
St Tropaz	325482
Stack	858885
Star Dust	A0A197
Stark White	D2C6B6
Starship	E3DD39
Steel Blue	4682B4
Steel Grey	43464B
Stiletto	833D3E
Stonewall	807661
Storm Dust	65645F
Storm Grey	747880
Straw	DABE82
Strikemaster	946A81
Stromboli	406356
Studio	724AA1
Submarine	8C9C9C
Sugar Cane	EEEFDF
Sulu	C6EA80
Summer Green	8FB69C
Summer Sky	38B0DE
Sun	EF8E38
Sundance	C4AA4D
Sundown	F8AFA9
Sunflower	DAC01A
Sunglo	C76155
Sunglow	FFCC33
Sunset	C0514A
Sunset Orange	FE4C40
Sunshade	FA9D49
Supernova	FFB437
Surf	B8D4BB
Surf Crest	C3D6BD
Surfie Green	007B77
Sushi	7C9F2F
Suva Grey	8B8685
Swamp	252F2F
Swans Down	DAE6DD
Sweet Corn	F9E176
Sweet Pink	EE918D
Swirl	D7CEC5
Swiss Coffee	DBD0CA
Tacao	F6AE78
Tacha	D2B960
Tahiti Gold	DC722A
Tahuna Sands	D8CC9B
Tall Poppy	853534
Tallow	A39977
Tamarillo	752B2F
Tan	D2B48C
Tana	B8B5A1
Tangaroa	1E2F3C
Tangerine	F28500
Tangerine Yellow	FFCC00
Tango	D46F31
Tapa	7C7C72
Tapestry	B37084
Tara	DEF1DD
Tarawera	253C48
Tasman	BAC0B3
Taupe	483C32
Taupe Grey	8B8589
Tawny Port	643A48
Tax Break	496569
Te Papa Green	2B4B40
Tea	BFB5A2
Tea Green	D0F0C0
Tea Rose	F883C2
Teak	AB8953
Teal	008080
Teal Blue	254855
Temptress	3C2126
Tenne (Tawny)	CD5700
Tequila	F4D0A4
Terra Cotta	E2725B
Texas	ECE67E
Texas Rose	FCB057
Thatch	B1948F
Thatch Green	544E31
Thistle	D8BFD8
Thunder	4D4D4B
Thunderbird	923830
Tia Maria	97422D
Tiara	B9C3BE
Tiber	184343
Tickle Me Pink	FC80A5
Tidal	F0F590
Tide	BEB4AB
Timber Green	324336
Timberwolf	D9D6CF
Titan White	DDD6E1
Toast	9F715F
Tobacco Brown	6D5843
Tobago	44362D
Toledo	3E2631
Tolopea	2D2541
Tom Thumb	4F6348
Tomato	FF6347
Tonys Pink	E79E88
Topaz	817C87
Torch Red	FD0E35
Torea Bay	353D75
Tory Blue	374E88
Tosca	744042
Tower Grey	9CACA5
Tradewind	6DAFA7
Tranquil	DDEDE9
Travertine	E2DDC7
Tree Poppy	E2813B
Trendy Green	7E8424
Trendy Pink	805D80
Trinidad	C54F33
Tropical Blue	AEC9EB
Tropical Rain Forest	00755E
Trout	4C5356
True V	8E72C7
Tuatara	454642
Tuft Bush	F9D3BE
Tulip Tree	E3AC3D
Tumbleweed	DEA681
Tuna	46494E
Tundora	585452
Turbo	F5CC23
Turkish Rose	A56E75
Turmeric	AE9041
Turquoise	40E0D0
Turquoise Blue	6CDAE7
Turtle Green	363E1D
Tuscany	AD6242
Tusk	E3E5B1
Tussock	BF914B
Tutu	F8E4E3
Twilight	DAC0CD
Twilight Blue	F4F6EC
Twine	C19156
Tyrian Purple	66023C
Ultra Pink	FF6FFF
Ultramarine	120A8F
Valencia	D4574E
Valentino	382C38
Valhalla	2A2B41
Van Cleef	523936
Vanilla	CCB69B
Vanilla Ice	EBD2D1
Varden	FDEFD3
Venetian Red	C80815
Venice Blue	2C5778
Venus	8B7D82
Verdigris	62603E
Verdun Green	48531A
Vermilion	FF4D00
Very Dark Brown	5C4033
Very Light Grey	CDCDCD
Vesuvius	A85533
Victoria	564985
Vida Loca	5F9228
Viking	4DB1C8
Vin Rouge	955264
Viola	C58F9D
Violent Violet	2E2249
Violet	EE82EE
Violet Blue	9F5F9F
Violet Red	F7468A
Viridian	40826D
Viridian Green	4B5F56
Vis Vis	F9E496
Vista Blue	97D5B3
Vista White	E3DFD9
Vivid Tangerine	FF9980
Vivid Violet	803790
Volcano	4E2728
Voodoo	443240
Vulcan	36383C
Wafer	D4BBB1
Waikawa Grey	5B6E91
Waiouru	4C4E31
Wan White	E4E2DC
Wasabi	849137
Water Leaf	B6ECDE
Watercourse	006E4E
Wattle	D6CA3D
Watusi	F2CDBB
Wax Flower	EEB39E
We Peep	FDD7D8
Wedgewood	4C6B88
Well Read	8E3537
West Coast	5C512F
West Side	E5823A
Westar	D4CFC5
Wewak	F1919A
Wheat	F5DEB3
Wheatfield	DFD7BD
Whiskey	D29062
Whiskey Sour	D4915D
Whisper	EFE6E6
White	FFFFFF
White Ice	D7EEE4
White Lilac	E7E5E8
White Linen	EEE7DC
White Nectar	F8F6D8
White Pointer	DAD6CC
White Rock	D4CFB4
White Smoke	F5F5F5
Wild Blue Yonder	7A89B8
Wild Rice	E3D474
Wild Sand	E7E4DE
Wild Strawberry	FF3399
Wild Watermelon	FD5B78
Wild Willow	BECA60
William	53736F
Willow Brook	DFE6CF
Willow Grove	69755C
Windsor	462C77
Wine Berry	522C35
Winter Hazel	D0C383
Wisp Pink	F9E8E2
Wisteria	C9A0DC
Wistful	A29ECD
Witch Haze	FBF073
Wood Bark	302621
Woodburn	463629
Woodland	626746
Woodrush	45402B
Woodsmoke	2B3230
Woody Brown	554545
Xanadu	75876E
Yellow	FFFF00
Yellow Green	9ACD32
Yellow Metal	73633E
Yellow Orange	FFAE42
Yellow Sea	F49F35
Your Pink	FFC5BB
Yukon Gold	826A21
Yuma	C7B882
Zambezi	6B5A5A
Zanah	B2C6B1
Zest	C6723B
Zeus	3B3C38
Ziggurat	81A6AA
Zinnwaldite	EBC2AF
Zircon	DEE3E3
Zombie	DDC283
Zorba	A29589
Zuccini	17462E
Zumthor	CDD5D5
