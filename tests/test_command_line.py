#!/usr/bin/env python3
# pylint: disable=missing-docstring
# -*- coding: utf-8 -*-
# Cspell: ignore Popen Caput Mortuum

import os
import subprocess


def test_cli_info():
    # Test missing --palette
    cmd = ["python", "create_gpl.py"]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res = process.communicate()[1]

    assert res
    assert "error" in res
    assert res.startswith("usage")

    cmd = ["python", "create_gpl.py", "-n", "2", "XXX"]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res = process.communicate()[1]

    assert res
    assert "error" in res
    assert res.startswith("usage")
    assert "--palette" in res

    # Test help argument
    cmd = ["python", "create_gpl.py", "-h"]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )

    res = process.communicate()[0]

    assert res
    assert res.startswith("usage")
    assert "-h, --help" in res
    assert "-p STR, --palette STR" in res

    # Test version argument
    cmd = ["python", "create_gpl.py", "-v"]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res = process.communicate()[0]

    assert res


def test_writing_file(tmpdir):
    base_colours = ["B85F22", "612D1F"]

    test_file = os.path.join(tmpdir, "test.txt")

    with open(test_file, "w") as f:
        f.write("\n".join(base_colours))

    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "2", test_file]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res = process.communicate()[0]
    res = res.split("\n")

    assert res[0] == "GIMP Palette"
    assert res[1].startswith("Name:")

    set1 = [
        " 92  48  17 Christine+50",
        "184  95  34 Christine",
        "220 175 144 Christine-50",
    ]
    set2 = [
        " 48  22  16 Caput Mortuum+50",
        " 97  45  31 Caput Mortuum",
        "176 150 143 Caput Mortuum-50",
    ]

    assert not set(set1).difference(set(res))
    assert not set(set2).difference(set(res))

    out_file = os.path.join(tmpdir, "test.gpl")
    cmd = [
        "python",
        "create_gpl.py",
        "-p",
        "A name",
        "-n",
        "2",
        "-o",
        out_file,
        test_file,
    ]
    subprocess.run(cmd)

    with open(out_file, "r") as f:
        res_out = [x.rstrip("\n") for x in f.readlines()]

    # We have an empty element in res - add it here too
    res_out = res_out + [""]

    assert not set(res_out).difference(set(res))


def test_grayscale_flag(tmpdir):
    base_colours = [""]

    test_file = os.path.join(tmpdir, "test.txt")

    with open(test_file, "w") as f:
        f.write("\n".join(base_colours))

    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "1", "-g", test_file]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res = process.communicate()[0]
    res = res.split("\n")

    assert res[0] == "GIMP Palette"
    assert res[1].startswith("Name:")

    expected = [
        "  0   0   0 Black",
        " 64  64  64 Gray+50",
        "127 127 127 Gray",
        "191 191 191 Gray-50",
        "255 255 255 White",
    ]

    expected = ["  0   0   0 Black", "127 127 127 Gray", "255 255 255 White"]

    assert not set(expected).difference(set(res))

    # Test with greater scales
    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "3", "-g", test_file]

    process = subprocess.Popen(
        cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    res = process.communicate()[0]
    res = res.split("\n")

    assert res[0] == "GIMP Palette"
    assert res[1].startswith("Name:")

    expected = [
        " 42  42  42 Gray+67",
        " 85  85  85 Gray+33",
        "127 127 127 Gray",
        "170 170 170 Gray-33",
        "212 212 212 Gray-67",
    ]

    assert not set(expected).difference(set(res))


def test_force_name():
    std_in_str = "B85F22 Colour name\n"

    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "1"]

    process = subprocess.Popen(
        cmd,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
    )

    res = process.communicate(input=std_in_str)[0]
    assert "Colour name" in res

    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "1", "-fn"]

    process = subprocess.Popen(
        cmd,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
    )

    res = process.communicate(input=std_in_str)[0]
    assert "Christine" in res


def test_no_colour_name():
    std_in_str = "B85F22\n"

    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "1", "-nc"]

    process = subprocess.Popen(
        cmd,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
    )

    res = process.communicate(input=std_in_str)[0]

    assert "Christine" not in res
    assert "\n184  95  34 \n" in res

    # But it is not removed if present
    std_in_str = "B85F22 Col name\n"

    process = subprocess.Popen(
        cmd,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
    )

    res = process.communicate(input=std_in_str)[0]
    assert "\n184  95  34 Col name\n" in res


def test_reading_glp_format():
    std_in_str = "GIMP Palette\n" "Name: A name\n" "184  95  34 Christine\n"

    cmd = ["python", "create_gpl.py", "-p", "A name", "-n", "1", "-fn"]

    process = subprocess.Popen(
        cmd,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE,
    )

    res = process.communicate(input=std_in_str)[0]
    assert "Christine" in res
    assert "GIMP Palette" in res
    assert "Name:" in res
