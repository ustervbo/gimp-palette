#!/usr/bin/env python3
# pylint: disable=missing-docstring
# -*- coding: utf-8 -*-

import create_gpl as cg
import string
import pytest


def test_get_named_colours():
    def is_hex(s):
        hex_digits = string.hexdigits
        return all(c in hex_digits for c in s)

    named_colours = cg.get_named_colours()

    assert isinstance(named_colours, dict)

    nc_keys = named_colours.keys()
    assert sum([is_hex(x) for x in nc_keys]) == len(nc_keys)


def test_hex2rgb():
    expected = [244, 159, 53]
    res = cg.hex2rgb("F49F35")

    assert isinstance(res, list)
    assert res == expected

    with pytest.raises(ValueError) as e:
        cg.hex2rgb("XXXYYY")

    assert "XX" in str(e.value)

    with pytest.raises(TypeError) as e:
        cg.hex2rgb(6)


def test_rgb2hex():
    expected = "F49F35"
    res = cg.rgb2hex((244, 159, 53))

    assert isinstance(res, str)
    assert res == expected.lower()

    res = cg.rgb2hex([244, 159, 53])
    assert isinstance(res, str)
    assert res == expected.lower()

    with pytest.raises(TypeError) as e:
        cg.rgb2hex([244, 159])

    assert "not enough arguments for format string" in str(e.value)

    with pytest.raises(TypeError) as e:
        cg.rgb2hex(1)

    assert "'int' object is not iterable" in str(e.value)

    with pytest.raises(TypeError) as e:
        cg.rgb2hex("1")

    assert "an integer is required, not str" in str(e.value)


def test_hex_to_col_name():
    res = cg.hex_to_col_name("F49F35")

    assert isinstance(res, str)
    assert res == "Zest"


def test_create_color_values():
    expectation = [["Zest-50", "facf9a"], ["Zest-100", "ffffff"]]
    res1 = cg.create_color_values(
        base_col="F49F35", col_name="Zest", num_steps=1, method="tint"
    )

    res2 = cg.create_color_values(
        base_col="F49F35", col_name="Zest", num_steps=2, method="tint"
    )

    assert isinstance(res1, list)
    assert res1 == [expectation[1]]

    assert isinstance(res2, list)
    assert res2 == expectation

    expectation = [["Zest+0", "f49f35"], ["Zest+50", "7a501a"]]
    res1 = cg.create_color_values(
        base_col="F49F35", col_name="Zest", num_steps=1, method="shade"
    )

    res2 = cg.create_color_values(
        base_col="F49F35", col_name="Zest", num_steps=2, method="shade"
    )

    assert isinstance(res1, list)
    assert res1 == [expectation[0]]

    assert isinstance(res2, list)
    assert res2 == expectation


def test_tint_and_shade():
    expectation = [["Zest+50", "7a501a"], ["Zest", "F49F35"], ["Zest-50", "facf9a"]]

    res1 = cg.tint_and_shade(col_name="Zest", col="F49F35", num_steps=1)

    res2 = cg.tint_and_shade(col_name="Zest", col="F49F35", num_steps=2)

    assert isinstance(res1, list)
    assert res1 == [expectation[1]]

    assert isinstance(res2, list)
    assert res2 == expectation
