#!/usr/bin/env python3
# pylint: disable=missing-docstring
# -*- coding: utf-8 -*-
# pylint: enable=missing-docstring


__author__ = "Ulrik Stervbo"
__contact__ = "ulrik.stervbo@gmail.com"
__copyright__ = "Ulrik Stervbo, 2019"
__license__ = "MIT"
__creation__ = "2019-May-02"
__version__ = "0.2"

__description__ = "Create GIMP Palette with tint and shade"

__summary__ = """
The script uses names collected by http://chir.ag/projects/name-that-color.

The GIMP Palette is as follows:

GIMP Palette
Name: a name
#
# Comments
#
# The colours are in RGB and last column is the name
#
  0   0   0  Black

If names are used (the default), the tints and shared are postfixed with -10, -20...,
-90, and +10, +20..., +90 respectively. 100% tint or shade is black and white
respectively and ignored.

"""

import argparse
import fileinput
import sys
import os


def create_parser() -> argparse.ArgumentParser:
    """Create the CLI argument parser

    Returns
    -------
    ArgumentParser
            The argument parser. See `~argparse.ArgumentParser`
    """

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__description__,
        epilog=(
            __summary__
            + "Written by {author} ({contact}).".format(
                author=__author__, contact=__contact__
            )
        ),
    )

    parser.add_argument(
        "file",
        nargs="*",
        help="specify input file. Can be a GIMP palette or a flat file of hex values ",
        metavar="FILE",
    )

    parser.add_argument(
        "-p",
        "--palette",
        type=str,
        dest="palette_name",
        action="store",
        help="the name of the palette. This will also be used for the filename",
        required=True,
        metavar="STR",
    )

    parser.add_argument(
        "-n",
        "--num-steps",
        type=int,
        dest="num_steps",
        action="store",
        default=10,
        help=(
            "the number of steps in the tint and in the shade."
            "Default to 10, which correspond to 10x 10%% shade, and 10x 10%% tint"
        ),
        metavar="INT",
    )

    parser.add_argument(
        "-nc",
        "--no-color-names",
        dest="use_named_colour",
        action="store_false",
        help=("should the name of the base colours be guessed?" "(Defaults to True)"),
    )

    parser.add_argument(
        "-fn",
        "--force-color-names",
        dest="force_names",
        action="store_true",
        help=(
            "force guessing the name of the base colours if they exist?"
            "(Defaults to False)"
        ),
    )

    parser.add_argument(
        "-g",
        "--add-grayscale",
        dest="add_grayscale",
        action="store_true",
        help=(
            "should the a grayscale be added? The scale starts at gray "
            "(rgb: 127, 127, 127; hex: 7F7F7F) and create '-n' colours"
            "in each direction, plus black and white. (Defaults to True)"
        ),
    )

    parser.add_argument("-o", "--output", help="the output file (Defaults to STDOUT)")

    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s {version}".format(version=__version__),
    )

    return parser


def read_file(options_file) -> list:
    """Read the colours from a file or stdin

    The file can be flat with hex values or a GIMP palette

    Parameters
    ----------
    path : str
        The file to read

    Returns
    -------
    arr
        The hex values of the colours
    """
    gpl_file = False
    ret_lst = list()

    for line in fileinput.input(files=options_file if options_file else ("-",)):
        line = line.strip()
        hex_value = None
        col_name = None

        if "GIMP Palette" in line:
            gpl_file = True

        if gpl_file:
            if line[0] == "#":
                continue

            rgb = line.split()
            if rgb[0].isdigit():  # Cspell: ignore isdigit
                hex_value = rgb2hex([int(x) for x in rgb[0:3]])
                col_name = " ".join(rgb[3:])
        else:
            rgb = line.split()
            hex_value = rgb[0]
            col_name = " ".join(rgb[1:])

        if hex_value:
            ret_lst.append([hex_value, col_name])

    return ret_lst


def get_named_colours() -> dict:
    """Dictionary of names colours

    Taken from 'view-source: http://chir.ag/projects/ntc/ntc.js'

    Returns
    -------
    dict
        key, value pairs of hex, colour name
    """

    named_colours = {}
    nc_file = os.path.join(os.getcwd(), "resources", "named_colours.txt")

    with open(nc_file, "r") as f:
        for line in f:
            line = line.strip()
            if not line.startswith("#"):
                line = line.split("\t")
                named_colours.update({line[1]: line[0]})

    return named_colours


def hex2rgb(hex_color: str) -> list:
    """Convert hex value to RGB list

    Parameters
    ----------
    hex_color : str
        The hex value to convert

    Returns
    -------
    list
        The RGB values
    """
    if hex_color[0] == "#":
        hex_color = hex_color[1:]
    hex_color = [hex_color[x : x + 2] for x in [0, 2, 4]]
    return [int(hex_value, 16) for hex_value in hex_color]


def rgb2hex(rgb_color: tuple) -> str:
    """Convert RBG to hex

    Parameters
    ----------
    rgb_color : tuple
        The RGB colour to convert. Must be a tuple or something that can be converted
         to a tuple such as a list

    Returns
    -------
    str
        [description]
    """
    if not isinstance(rgb_color, tuple):
        rgb_color = tuple(rgb_color)
    return "%02x%02x%02x" % rgb_color


def hex_to_col_name(hex_color: str) -> str:
    """Get the name of a colour

  Parameters
  ----------
  hex_color : str
      The hex value of the colour to name

  Returns
  -------
  str
      The name of the colour
  """

    col_names_dict = get_named_colours()

    match_diff = None
    match_col_name = None

    col_rgb = hex2rgb(hex_color)

    for key, value in col_names_dict.items():
        cur_col_rgb = hex2rgb(key)
        red_dist = (cur_col_rgb[0] - col_rgb[0]) ** 2
        green_dist = (cur_col_rgb[1] - col_rgb[1]) ** 2
        blue_dist = (cur_col_rgb[2] - col_rgb[2]) ** 2
        rgb_d = red_dist + green_dist + blue_dist

        if not match_diff or match_diff > rgb_d:
            match_diff = rgb_d
            match_col_name = value

    return match_col_name


# pylint: disable=bad-continuation
def create_color_values(
    base_col: str, col_name: str, num_steps: int, method: str
) -> list:
    """Create a number of shade or tints from base colour

    The base colour is included in the shading result but absent in the tinting result.

    Parameters
    ----------
    base_col : str
        The base hex colour to tint or shade from
    col_name : str
        The name of the base colour
    num_steps : int
        The number of steps to tint/shade by
    method : str
        'shade' or 'tint'

    Returns
    -------
    list
        A list of tints and shades. Each element is a list with two elements, the
        colour identifier at position 0, and a hex representation of the colour at
        position 1
    """
    accepted_method = {"shade", "tint"}
    if method not in accepted_method:
        raise ValueError("method must be one of %r." % accepted_method)

    factor = 1 / num_steps
    steps = list(range(1, num_steps + 1))
    base_rgb = hex2rgb(hex_color=base_col)
    ret_lst = list()

    if method == "shade":
        separator = "+"
    else:
        separator = "-"

    for step in steps:
        cur_factor = factor * step

        if method == "tint":
            # Tint
            col_value = [round(cv + ((255 - cv) * cur_factor)) for cv in base_rgb]
            col_value_name = col_name + separator + str(round(cur_factor * 100))
        else:
            # Shade
            col_value = [round(cv * cur_factor) for cv in base_rgb]
            col_value_name = col_name + separator + str(round((1 - cur_factor) * 100))

        ret_lst.append([col_value_name, rgb2hex(col_value)])

    if method == "shade":
        ret_lst = list(reversed(ret_lst))
    else:
        ret_lst = list(ret_lst)

    return ret_lst


def tint_and_shade(col_name: str, col: str, num_steps: int) -> list:
    """Tint and Shade colour

    The number of steps are between the colour and black, and between the colour and
    white. The length of the dict is

    Parameters
    ----------
    col_name : list
        The identifier of the colour to tint and shade

    col : str
        The hex colour to tint and shade

    num_steps : int
        The number of steps in each direction
    Returns
    -------
    list
        A list of tints and shades. Each element is a list with two elements, the
        colour identifier at position 0, and a hex representation of the colour at
        position 1
    """

    # Tint and Shades
    tints = create_color_values(
        base_col=col, col_name=col_name, num_steps=num_steps, method="tint"
    )[: num_steps - 1]
    shades = create_color_values(
        base_col=col, col_name=col_name, num_steps=num_steps, method="shade"
    )[1:]
    shades = list(reversed(shades))

    return shades + [[col_name, col]] + tints


def print_gimp_palette(palette_colors: list, palette_name: str):
    """Print the palette as a GIMP Palette

    Parameters
    ----------
    palette_colors : list
        The colors to print. Each element is a list with two elements, the colour
        identifier at position 0, and a hex representation of the colour at position 1
    palette_name : str
        The name of the palette
    """
    print("GIMP Palette")
    print("Name: " + palette_name)
    print("#")
    print("# Generated with Create GIMP Palette with tint and shade")
    print("#")

    for base_col in palette_colors:
        for col in base_col:
            col_name = col[0]

            col_rgb = hex2rgb(col[1])

            col_r = "{:>3}".format(col_rgb[0])
            col_g = "{:>3}".format(col_rgb[1])
            col_b = "{:>3}".format(col_rgb[2])

            print(" ".join([col_r, col_g, col_b, col_name]))


def main():
    """Make magic
    """
    parser = create_parser()
    options = parser.parse_args()

    base_colours = read_file(options.file)
    shaded_tinted_colours = list()

    for base_col in base_colours:
        col = base_col[0].replace("#", "")
        col_name = base_col[1]  # "#" + col

        if options.force_names:
            col_name = hex_to_col_name(col)
        elif not col_name and options.use_named_colour:
            col_name = hex_to_col_name(col)

        shaded_tinted_colours.append(tint_and_shade(col_name, col, options.num_steps))

    if options.add_grayscale:
        gray_scale = tint_and_shade("Gray", "7f7f7f", options.num_steps)
        gray_scale = [["Black", "000000"]] + gray_scale + [["White", "ffffff"]]
        shaded_tinted_colours = [gray_scale] + shaded_tinted_colours

    if options.output and options.output != "-":
        sys.stdout = open(options.output, "w")

    print_gimp_palette(
        palette_colors=shaded_tinted_colours, palette_name=options.palette_name
    )


if __name__ == "__main__":
    main()
