<!--
  cspell:ignore Caput Mortuum
-->

# Create GIMP Palette with tint and shade

I sometimes have to generate a GIMP palette to use with Inkscape. The approach includes finding nice palette (Pantone or [http://colormind.io/](http://colormind.io/)), creating an number of shades and tints using [https://maketintsandshades.com/](8https://maketintsandshades.com/) and manually combining it all into a GIMP palette.

After some time of creation, and above all re-creation, I had enough and put together this python script to create tints and shades of colours in GIMP palette format. The script can even name the base colours of the colour scheme. And that all off-line.

## Examples

Save the colours:

```txt
B85F22
612D1F
705043
7499A8
B7BAAD
```

to a file with a great name. 'test.txt' for instance.

Create a palette with 5 tints and shades in each direction:

```bash
cat test.txt | python create_gpl.py -p 'A Name' -n 5
```

or

```bash
python create_gpl.py -p 'A Name' -n 5 test.txt
```

`-p` gives the name of the palette, and `n` the number of tints and shades. The `-g` option add a gray scale:

```bash
cat test.txt | python create_gpl.py -p 'A Name' -n 5 -g
```

and the `-nc` is you prefer to not use approximate colour names

```bash
cat test.txt | python create_gpl.py -p 'A Name' -n 5 -nc
```

Of course you can write to a file too by using the option `-o`

```bash
cat test.txt | python create_gpl.py -p 'A Name' -n 5 -g -o test.gpl
```

If all went well, you will now have a file with the following content:

```txt
GIMP Palette
Name: A Name
#
# Generated with Create GIMP Palette with tint and shade
#
  0   0   0 Black
 25  25  25 Gray+80
 51  51  51 Gray+60
 76  76  76 Gray+40
102 102 102 Gray+20
127 127 127 Gray
153 153 153 Gray-20
178 178 178 Gray-40
204 204 204 Gray-60
229 229 229 Gray-80
255 255 255 White
 37  19   7 Christine+80
 74  38  14 Christine+60
110  57  20 Christine+40
147  76  27 Christine+20
184  95  34 Christine
198 127  78 Christine-20
212 159 122 Christine-40
227 191 167 Christine-60
241 223 211 Christine-80
 19   9   6 Caput Mortuum+80
 39  18  12 Caput Mortuum+60
 58  27  19 Caput Mortuum+40
 78  36  25 Caput Mortuum+20
 97  45  31 Caput Mortuum
129  87  76 Caput Mortuum-20
160 129 121 Caput Mortuum-40
192 171 165 Caput Mortuum-60
223 213 210 Caput Mortuum-80
 22  16  13 Spice+80
 45  32  27 Spice+60
 67  48  40 Spice+40
 90  64  54 Spice+20
112  80  67 Spice
141 115 105 Spice-20
169 150 142 Spice-40
198 185 180 Spice-60
226 220 217 Spice-80
 23  31  34 Neptune+80
 46  61  67 Neptune+60
 70  92 101 Neptune+40
 93 122 134 Neptune+20
116 153 168 Neptune
144 173 185 Neptune-20
172 194 203 Neptune-40
199 214 220 Neptune-60
227 235 238 Neptune-80
 37  37  35 Mist Grey+80
 73  74  69 Mist Grey+60
110 112 104 Mist Grey+40
146 149 138 Mist Grey+20
183 186 173 Mist Grey
197 200 189 Mist Grey-20
212 214 206 Mist Grey-40
226 227 222 Mist Grey-60
241 241 239 Mist Grey-80
```

Existing names will automatically be preserved.

Save the colours:

```txt
B85F22
#612D1F Col B

```

to a file with a great name - again it seems 'test.txt' is a good pick. Running

```bash
python create_gpl.py -p 'A Name' -n 3 test.txt
```

should give the palette, where the colour name Christine is guessed, but Col B is preserved.

```txt
GIMP Palette
Name: A Name
#
# Generated with Create GIMP Palette with tint and shade
#
 61  32  11 Christine+67
123  63  23 Christine+33
184  95  34 Christine
208 148 108 Christine-33
231 202 181 Christine-67
 32  15  10 Col B+67
 65  30  21 Col B+33
 97  45  31 Col B
150 115 106 Col B-33
202 185 180 Col B-67
```

The command

```bash
python create_gpl.py -p 'A Name' -fn -n 3 test.txt
```

will guess the name of both colours.

## Thank you

* [https://maketintsandshades.com/](https://maketintsandshades.com/) for inspiration
* [http://colormind.io/](http://colormind.io/) for all the great palettes
* [http://chir.ag/](http://chir.ag/) for the collection of colour names
